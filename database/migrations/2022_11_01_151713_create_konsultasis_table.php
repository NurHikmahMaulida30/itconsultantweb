<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKonsultasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konsultasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama',100);
            $table->string('email')->unique();
            $table->string('metode_konsultasi',100);
            $table->date('tanggal_konsultasi',100);
            $table->unsignedBigInteger('konsultan_id');
            $table->timestamps();

            $table->foreign('konsultan_id')->references('id')->on('daftar_konsultans');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konsultasis');
    }
}
