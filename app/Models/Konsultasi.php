<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Konsultasi extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','nama','email','metode_konsultasi','tanggal_konsultasi','konsultan_id'

     ];
 
     protected $hidden = [
        
     ];

     public function konsultasi(){
        return $this->belongsTo(daftar_konsultan::class,'id');
        
    }

}
