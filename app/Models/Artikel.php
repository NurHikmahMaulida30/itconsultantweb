<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','nama_id','judul_artikel','isi_artikel','gambar'

     ];
 
     protected $hidden = [
        
     ];


    public function user(){
      return $this->belongsTo(User::class,'id');
      
  }

}
