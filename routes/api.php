<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ArtitikelApiController;
use App\Http\Controllers\Api\NamaKonsultanApiController;
use App\Http\Controllers\Api\KonsultasiApiController;
use App\Http\Controllers\Api\LoginApiController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/artikel', [ArtitikelApiController::class, 'index'])->name('artikel');
Route::post('/input_artikel', [ArtitikelApiController::class, 'store'])->name('input_artikel');

Route::get('/konsultan', [NamaKonsultanApiController::class, 'index'])->name('konsultan');
Route::post('/input_konsultan', [NamaKonsultanApiController::class, 'store'])->name('input_konsultan');


Route::get('/konsultasi', [KonsultasiApiController::class, 'index'])->name('konsultasi');
Route::post('/input_konsultasi', [KonsultasiApiController::class, 'store'])->name('input_konsultasi');

Route::get('/DataUser', [LoginApiController::class, 'index'])->name('DataUser');
Route::post('/Register', [LoginApiController::class, 'store'])->name('Register');
Route::post('/Login', [LoginApiController::class, 'Login'])->name('Login');